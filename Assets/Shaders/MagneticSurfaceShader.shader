Shader "Custom/MagneticSurfaceShader"
{
	// Defining the main properties as exposed in the inspector
	Properties
	{
		_BaseColor ("Base Color", Color) = (1,1,1,1)
		_ProximityColor ("Proximity Color", Color) = (0,0,0,1)
		_BallRadius ("Ball Radius", float) = 5
		_BaseAlpha ("Base Alpha", float) = 0
		_ProximityAlpha ("Proximity Alpha", float) = 0.8
	}

	

	// start first subshader (there is only one, but there could be multiple)
	SubShader
	{
		Tags {"Queue"="Transparent" "RenderType" = "Transparent"} // other render types are "Transparent" or "Geometry", defines when stuff gets rendered.

		

		Pass // A shader can have multible passes. One pass = one time render everything.


		{
			ZWrite Off
	        Blend SrcAlpha OneMinusSrcAlpha
	        Cull Off 
	        LOD 100

			
			CGPROGRAM // start a section of CG code
			#pragma vertex vertexShader // define the vertex shader function
			#pragma fragment fragmentShader // define the pixel shader function
			
			#include "UnityCG.cginc" //has many helpful functions

			//******* everything above here is just setup, you can ignore that ********

			float4 _BaseColor;
			float4 _ProximityColor;
			float _BallRadius;
			float _BaseAlpha;
			float _ProximityAlpha;
			int _BallCount = 0;
			float4 _Balls[500];

			// struct defining the Input for the VertexShader
			struct vertexInput
			{
				float4 position : POSITION; // The : POSITION means what semantic to put this variable in. (what it is intendet for)
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			// struct defining the Output of the VertexShader and Input for the FragmentShader
			struct vertexOutput
			{
				float4 position : SV_POSITION;
				float4 worldPos : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			vertexOutput vertexShader (vertexInput vInput)
			{
				vertexOutput vOutput;
				UNITY_SETUP_INSTANCE_ID(vInput);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(vOutput);

				// the vertex data is comming in in local space, we need to transform it into clip space!
				// first into world space:
				// (unity_ObjectToWorld is UNITY_MATRIX_M)

				vOutput.position = mul(unity_ObjectToWorld, vInput.position);
				vOutput.worldPos = vOutput.position;		
				// then into view space:
				vOutput.position = mul(UNITY_MATRIX_V, vOutput.position);
				// finally via projection into clip space! (the cube)
				vOutput.position = mul(UNITY_MATRIX_P, vOutput.position);

				// Or do all operations with the combined ModelViewProjection Matrix!:
				// vOutput.position = mul(UNITY_MATRIX_MVP, vInput.position);

				return vOutput;
			}
			
			fixed4 fragmentShader (vertexOutput vOutput) : SV_Target
			{

				fixed4 col = vector(_BaseColor.x, _BaseColor.y, _BaseColor.z, _BaseAlpha);
				for (int i = 0; i < _BallCount; i++) {
					if(_Balls[i].x == 0 && _Balls[i].y ==0 && _Balls[i].z ==0)
					{
						break;
					}
					float dist = sqrt(pow(_Balls[i].x - vOutput.worldPos.x,2) +
									   pow(_Balls[i].y - vOutput.worldPos.y,2) +
									   pow(_Balls[i].z - vOutput.worldPos.z,2));

					if (_BallRadius + 1 >= dist)
					{
						col = vector(_ProximityColor.x, _ProximityColor.y, _ProximityColor.z, _ProximityAlpha);
					}
				}
				return col;
			}

			ENDCG
		}
	}
}