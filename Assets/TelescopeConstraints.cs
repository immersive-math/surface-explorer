using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class TelescopeConstraints : MonoBehaviour
{
    public GameObject telescope;
    public GameObject telescopePivot;
    public GameObject zoomButton;
    public GameObject handle;
    public Camera telescopeCamera;

    public float minTiltTelescope;
    public float maxTiltTelescope;
    
    public float minTiltZoomButton;
    public float maxTiltZoomButton;

    private float zoom1xFOV;
    private float zoom2xFOV;
    private float zoom4xFOV;
    private float zoom8xFOV;
    private float zoom16xFOV;

    private float zoom1xAngle;
    private float zoom2xAngle;
    private float zoom4xAngle;
    private float zoom8xAngle;
    private float zoom16xAngle;

    private bool _rotatingTelescope = false;
    private Vector3 _handleCenter;
    private Vector3 _zoomButtonCenter;
    private Vector3 _lastHandPos;
    private IXRSelectInteractor _hand;
    private bool _rotatingZoomButton;

    private void Start()
    {
        float fovDegrees = telescopeCamera.fieldOfView;
        float fovRadians = (fovDegrees / 180) * Mathf.PI;
        zoom1xFOV = telescopeCamera.fieldOfView;
        zoom2xFOV = ((Mathf.Atan(Mathf.Tan(fovRadians / 2) / 2)) / Mathf.PI) * 360;
        zoom4xFOV = ((Mathf.Atan(Mathf.Tan(fovRadians / 2) / 4)) / Mathf.PI) * 360;
        zoom8xFOV = ((Mathf.Atan(Mathf.Tan(fovRadians / 2) / 8)) / Mathf.PI) * 360;
        zoom16xFOV = ((Mathf.Atan(Mathf.Tan(fovRadians / 2) / 16)) / Mathf.PI) * 360;

        zoom1xAngle = minTiltZoomButton;
        zoom2xAngle = minTiltZoomButton + (maxTiltZoomButton - minTiltZoomButton) / 4;
        zoom4xAngle = minTiltZoomButton + 2 * (maxTiltZoomButton - minTiltZoomButton) / 4;
        zoom8xAngle = minTiltZoomButton + 3 * (maxTiltZoomButton - minTiltZoomButton) / 4;
        zoom16xAngle = maxTiltZoomButton;
        
        Debug.Log(zoom1xAngle + " " + zoom2xAngle + " " + zoom4xAngle + " " + zoom8xAngle + " " + zoom16xAngle);
    }

    private void Update()
    {
        if (_rotatingTelescope)
        {
            Vector3 oldPivotVec = Vector3.ProjectOnPlane(_lastHandPos - _handleCenter, 
                telescopePivot.transform.forward).normalized;
            Vector3 newPivotVec = Vector3.ProjectOnPlane(_hand.transform.position - _handleCenter, 
                telescopePivot.transform.forward).normalized;
            
            Vector3 oldTelescopeVec = Vector3.ProjectOnPlane(_lastHandPos - _handleCenter, 
                telescope.transform.up).normalized;
            Vector3 newTelescopeVec = Vector3.ProjectOnPlane(_hand.transform.position - _handleCenter, 
                telescope.transform.up).normalized;
            oldTelescopeVec = Vector3.Lerp(oldTelescopeVec, newTelescopeVec, 0.90f);
            
            telescope.transform.rotation = Quaternion.FromToRotation(oldTelescopeVec,newTelescopeVec) * telescope.transform.rotation;
            if (telescope.transform.localRotation.eulerAngles.y > maxTiltTelescope)
            {
                telescope.transform.SetLocalPositionAndRotation(telescope.transform.localPosition, 
                    Quaternion.Euler(telescope.transform.localRotation.eulerAngles.x, 
                    maxTiltTelescope, 
                    telescope.transform.localRotation.eulerAngles.z)); 
            }
            else if (telescope.transform.localRotation.eulerAngles.y < minTiltTelescope)
            {
                telescope.transform.SetLocalPositionAndRotation(telescope.transform.localPosition, 
                    Quaternion.Euler(telescope.transform.localRotation.eulerAngles.x, 
                        minTiltTelescope, 
                        telescope.transform.localRotation.eulerAngles.z)); 
            }
            
            telescopePivot.transform.rotation = Quaternion.FromToRotation(oldPivotVec,newPivotVec) * telescopePivot.transform.rotation;
            _lastHandPos = _hand.transform.position;
        }

        if (_rotatingZoomButton)
        {
            Vector3 oldZoomButtonVec = Vector3.ProjectOnPlane(_lastHandPos - _zoomButtonCenter, 
                zoomButton.transform.right).normalized;
            Vector3 newZoomButtonVec = Vector3.ProjectOnPlane(_hand.transform.position - _zoomButtonCenter, 
                zoomButton.transform.right).normalized;
            
            zoomButton.transform.rotation = Quaternion.FromToRotation(oldZoomButtonVec,newZoomButtonVec) * zoomButton.transform.rotation;
            // Rotating the zoom button in negative direction gives us the max zoom (0 to -26)
            // Thus the inequality signs are flipped (compared to telescope tilt)
            if (zoomButton.transform.localRotation.eulerAngles.x < 180)
            {
                zoomButton.transform.SetLocalPositionAndRotation(zoomButton.transform.localPosition, 
                    Quaternion.Euler(zoom1xAngle,
                        zoomButton.transform.localRotation.eulerAngles.y,
                        zoomButton.transform.localRotation.eulerAngles.z)); 
                telescopeCamera.fieldOfView = zoom1xFOV;
            }
            else if (zoomButton.transform.localRotation.eulerAngles.x < 360 - maxTiltZoomButton)
            {
                zoomButton.transform.SetLocalPositionAndRotation(zoomButton.transform.localPosition, 
                    Quaternion.Euler(360 - zoom16xAngle,
                        zoomButton.transform.localRotation.eulerAngles.y,
                        zoomButton.transform.localRotation.eulerAngles.z));
                telescopeCamera.fieldOfView = zoom16xFOV;
            }
            _lastHandPos = _hand.transform.position;
        }
    }
    
    public void StartZoomButtonRotation(SelectEnterEventArgs args)
    {
        if (_rotatingZoomButton || _rotatingTelescope) { return; }
        _rotatingZoomButton = true;
        _zoomButtonCenter = zoomButton.transform.position;
        _lastHandPos = args.interactorObject.transform.position;
        _hand = args.interactorObject;
    }
    
    public void StopZoomButtonRotation()
    {
        _rotatingZoomButton = false;
        Vector3 adjustedRotation = zoomButton.transform.localEulerAngles - new Vector3(180, 0, 0);
        if (adjustedRotation.x > 180 - (zoom1xAngle + zoom2xAngle)/2 || adjustedRotation.x < 0)
        {
            zoomButton.transform.SetLocalPositionAndRotation(zoomButton.transform.localPosition, 
                Quaternion.Euler(360 - zoom1xAngle,
                    zoomButton.transform.localRotation.eulerAngles.y,
                    zoomButton.transform.localRotation.eulerAngles.z)); 
            telescopeCamera.fieldOfView = zoom1xFOV;
        }
        else if (adjustedRotation.x > 180 - (zoom2xAngle + zoom4xAngle)/2)
        {
            zoomButton.transform.SetLocalPositionAndRotation(zoomButton.transform.localPosition, 
                Quaternion.Euler(360 - zoom2xAngle,
                    zoomButton.transform.localRotation.eulerAngles.y,
                    zoomButton.transform.localRotation.eulerAngles.z)); 
            telescopeCamera.fieldOfView = zoom2xFOV;
        }
        else if (adjustedRotation.x > 180 - (zoom4xAngle + zoom8xAngle)/2)
        {
            zoomButton.transform.SetLocalPositionAndRotation(zoomButton.transform.localPosition, 
                Quaternion.Euler(360 - zoom4xAngle,
                    zoomButton.transform.localRotation.eulerAngles.y,
                    zoomButton.transform.localRotation.eulerAngles.z));
            telescopeCamera.fieldOfView = zoom4xFOV;
        }
        else if (adjustedRotation.x > 180 - (zoom8xAngle + zoom16xAngle)/2)
        {
            zoomButton.transform.SetLocalPositionAndRotation(zoomButton.transform.localPosition, 
                Quaternion.Euler(360 - zoom8xAngle,
                    zoomButton.transform.localRotation.eulerAngles.y,
                    zoomButton.transform.localRotation.eulerAngles.z)); 
            telescopeCamera.fieldOfView = zoom8xFOV;
        }
        else
        {
            zoomButton.transform.SetLocalPositionAndRotation(zoomButton.transform.localPosition, 
                Quaternion.Euler(360 - zoom16xAngle,
                    zoomButton.transform.localRotation.eulerAngles.y,
                    zoomButton.transform.localRotation.eulerAngles.z));
            telescopeCamera.fieldOfView = zoom16xFOV;
        }
    }

    public void StartTelescopeRotation(SelectEnterEventArgs args)
    {
        if (_rotatingTelescope || _rotatingZoomButton) { return; }
        _rotatingTelescope = true;
        _handleCenter = handle.transform.position;
        _lastHandPos = args.interactorObject.transform.position;
        _hand = args.interactorObject;
    }

    public void StopTelescopeRotation()
    {
        _rotatingTelescope = false;
        
    }
}
