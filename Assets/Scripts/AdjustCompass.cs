using MagneticSurfaces;
using UnityEngine;

public class AdjustCompass : MonoBehaviour
{
    public GameObject player;

    public void AdjustCompassDirection()
    {
        transform.eulerAngles = new Vector3(0, 0, 90);

        Vector3 northPoint = new Vector3(0, 100000, 0);
        Vector3 playerPos = player.transform.position;
        Vector3 mappedPos = FindObjectOfType<MagneticSurface>().MapToEdge(playerPos, 0);
        Vector3 normal = FindObjectOfType<MagneticSurface>().SurfaceNormal(mappedPos);
        Vector3 northDirection = Vector3.ProjectOnPlane(northPoint - playerPos, normal).normalized;
        Quaternion rot = Quaternion.FromToRotation(normal, northDirection);
        transform.rotation = rot * transform.rotation;
    }
}