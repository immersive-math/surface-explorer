using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelescopeManager : MonoBehaviour
{
    private static TelescopeManager _instance;

    public static TelescopeManager Instance
    {
        get { return _instance; }
    }
    
    [SerializeField] private Camera telescopeCamera;
    
    public float _minZoom = 1f;
    public float _maxZoom = 10f;
    private float _zoomAmount;
    public bool _telescopeEnabled;

    public GameObject telescope;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    
    private void Start()
    {
        GameManager.Instance.toggleTelescopeEvent.AddListener(ToggleTelescope);
    }

    public void IncreaseZoom()
    {
        _zoomAmount += 1;
        _zoomAmount = Mathf.Min(_maxZoom, _zoomAmount);
        telescopeCamera.fieldOfView = 60 * Mathf.Atan(1 / _zoomAmount);
    }
    
    public void DecreaseZoom()
    {
        _zoomAmount -= 1;
        _zoomAmount = Mathf.Max(_minZoom, _zoomAmount);
        telescopeCamera.fieldOfView = 60 * Mathf.Atan(1 / _zoomAmount);
    }

    public void ToggleTelescope()
    {
        _telescopeEnabled = !_telescopeEnabled;
        if (_telescopeEnabled)
            EnableTelescope();
        else
            DisableTelescope();
    }

    private void EnableTelescope()
    {
        telescope.SetActive(true);
    }

    private void DisableTelescope()
    {
        telescope.SetActive(false);
    }
}
