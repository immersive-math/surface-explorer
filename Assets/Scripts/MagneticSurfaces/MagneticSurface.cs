﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.XR.Interaction.Toolkit;

namespace MagneticSurfaces
{
    [RequireComponent(typeof(MeshCollider))]
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public abstract class MagneticSurface : TeleportationArea
    {
        [FormerlySerializedAs("n")] public int vertexGen_n = 5;

        protected Vector3 center => this.transform.position;
        protected internal List<Transform> balls = new List<Transform>();
        public Material magneticSurfaceMaterial;

        protected void Start()
        {
            MeshFilter mf = GetComponent<MeshFilter>();
            Mesh mesh = mf.mesh;
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            mesh.vertices = Vertices();
            mesh.triangles = Triangles();
            //mesh.uvs = UV();
            mesh.RecalculateNormals();
            MeshCollider mc = GetComponent<MeshCollider>();
            mc.sharedMesh = mesh;
            mc.providesContacts = true;
            mc.convex = false;
            magneticSurfaceMaterial = GetComponent<MeshRenderer>().material;
            magneticSurfaceMaterial.SetFloat("_BallCount", 500);
            GameManager.Instance.toggleNeighborhoodsEvent.AddListener(ToggleNeighborhoods);
        }

        protected void Update()
        {
            Vector4[] b = new Vector4[500];

            for (int i = 0; i < Math.Min(balls.Count, 500); i++)
            {
                b[i] = balls[i].position;
            }

            magneticSurfaceMaterial.SetVectorArray("_Balls", b);
        }

        // This handles teleporting
        protected override void OnSelectExiting(SelectExitEventArgs args)
        {
            base.OnSelectExiting(args);

            Transform xrOrigin = GameManager.Instance.XROrigin;
            RaycastHit hit;
            args.interactorObject.transform.GetComponent<XRRayInteractor>().TryGetCurrent3DRaycastHit(out hit);
            if (hit.point != Vector3.zero)
            {
                HandleTeleport(xrOrigin, hit.point, 0.2f);
            }
        }
        
        protected override bool GenerateTeleportRequest(IXRInteractor interactor, RaycastHit raycastHit, ref TeleportRequest teleportRequest)
        {
            return false;
        }

        public virtual void HandleTeleport(Transform xrorigin, Vector3 position, float offset)
        {
            Vector3 newPos = MapToEdge(position, offset);
            Vector3 normal = AdjustedSurfaceNormal(newPos);

            xrorigin.position = newPos;
            xrorigin.up = normal;
        }

        protected internal abstract Vector3 SurfaceNormal(Vector3 objectTransformPosition);

        protected internal Vector3 AdjustedSurfaceNormal(Vector3 objectTransformPosition)
        {
            float scalar = 1;
            if (!IsOutside(objectTransformPosition))
            {
                scalar = -1;
            }

            return SurfaceNormal(objectTransformPosition) * scalar;
        }

        protected internal abstract Vector3 MapToEdge(Vector3 objectTransformPosition, float offset);
        protected abstract bool IsOutside(Vector3 objectTransformPosition);

        protected abstract Vector3[] Vertices();

        protected abstract int[] Triangles();


        protected int getVertexIndex(int i, int j, int n, int m)
        {
            return n * (i % n) + (j % m);
        }

        protected int[] getQuad(int i, int j, int n, int m = -1)
        {
            if (m == -1)
            {
                m = n;
            }

            //wrap on i and j
            // v_i,j -- v_(i,j+1 %vertexGen_n)
            //  |             |
            //  |             |
            // v_(i+1,j) -- v(i+1 %vertexGen_n, j+1 %vertexGen_n)
            int[] quad_tris = new int[6];
            quad_tris[0] = getVertexIndex(i, j, n, m);
            quad_tris[1] = getVertexIndex(i, j + 1, n, m);
            quad_tris[2] = getVertexIndex(i + 1, j + 1, n, m);
            quad_tris[3] = getVertexIndex(i, j, n, m);
            quad_tris[4] = getVertexIndex(i + 1, j + 1, n, m);
            quad_tris[5] = getVertexIndex(i + 1, j, n, m);
            return quad_tris;
        }
        
        protected int[] getQuadInverseNormals(int i, int j, int n, int m = -1)
        {
            if (m == -1)
            {
                m = n;
            }

            //wrap on i and j
            // v_i,j -- v_(i,j+1 %vertexGen_n)
            //  |             |
            //  |             |
            // v_(i+1,j) -- v(i+1 %vertexGen_n, j+1 %vertexGen_n)
            int[] quad_tris = new int[6];
            quad_tris[0] = getVertexIndex(i, j, n, m);
            quad_tris[1] = getVertexIndex(i + 1, j + 1, n, m);
            quad_tris[2] = getVertexIndex(i, j + 1, n, m);
            quad_tris[3] = getVertexIndex(i, j, n, m);
            quad_tris[4] = getVertexIndex(i + 1, j, n, m);
            quad_tris[5] = getVertexIndex(i + 1, j + 1, n, m);
            return quad_tris;
        }

        private void ToggleNeighborhoods()
        {
            GameManager.neighborhoodsEnabled = !GameManager.neighborhoodsEnabled;
            magneticSurfaceMaterial.SetFloat("_ProximityAlpha", GameManager.neighborhoodsEnabled ? 0.5f : 0f);
        }

        public void RefreshBallList()
        {
            balls = new List<Transform>();
        }
    }
}