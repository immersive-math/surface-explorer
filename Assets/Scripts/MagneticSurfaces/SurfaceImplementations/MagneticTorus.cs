﻿using UnityEngine;

namespace MagneticSurfaces.SurfaceImplementations
{
    public class MagneticTorus : MagneticSurface
    {
        public float majorRadius = 2f;

        public float minorRadius = 0.5f;

        //TODO fix blender output
        private Vector3 majorPlaneNormal => this.transform.up;

        protected override Vector3[] Vertices()
        {
            Vector3[] verts = new Vector3[vertexGen_n * vertexGen_n];

            Vector3 secondaryAxisReference = this.transform.right;
            Vector3 tertiaryAxisReference = this.transform.forward;
            for (int i = 0; i < vertexGen_n; i++)
            {
                float majorAngle = 360f * i / vertexGen_n;
                Quaternion q1 = Quaternion.AngleAxis(majorAngle, majorPlaneNormal);
                Vector3 localCenter = (q1 * secondaryAxisReference) * majorRadius + center;
                for (int j = 0; j < vertexGen_n; j++)
                {
                    float minorAngle = 360f * j / vertexGen_n;
                    Quaternion q2 = Quaternion.AngleAxis(minorAngle, q1 * tertiaryAxisReference);
                    verts[vertexGen_n * i + j] = localCenter + (q2 * (q1 * secondaryAxisReference)) * minorRadius;
                }
            }

            return verts;
        }

        protected override int[] Triangles()
        {
            int[] tris = new int[6 * (vertexGen_n * vertexGen_n)];
            for (int i = 0; i < vertexGen_n; i++)
            {
                for (int j = 0; j < vertexGen_n; j++)
                {
                    int idx = 6 * (vertexGen_n * i + j);
                    System.Array.Copy(getQuadInverseNormals(i, j, vertexGen_n), 0, tris, idx, 6);
                    // source,source-index,dest,dest-index,count
                }
            }

            return tris;
        }

        protected internal override Vector3 SurfaceNormal(Vector3 objectTransformPosition)
        {
            Vector3 localGravityCenter = GetLocalGravityCenter(objectTransformPosition);
            Vector3 gravityDirection = objectTransformPosition - localGravityCenter;
            return gravityDirection.normalized;
        }

        protected internal override Vector3 MapToEdge(Vector3 objectTransformPosition, float offset)
        {
            // find the center of a circular cross-section of the torus
            Vector3 localGravityCenter = GetLocalGravityCenter(objectTransformPosition);
            if (GameManager.IsOutside())
            {
                //outside the sphere
                offset *= -1;
            }

            return (objectTransformPosition - localGravityCenter).normalized * (minorRadius - offset) +
                   localGravityCenter;
        }

        internal Vector3 GetLocalGravityCenter(Vector3 transformPosition)
        {
            return center +
                   Vector3.ProjectOnPlane(transformPosition - center, majorPlaneNormal).normalized *
                   majorRadius;
        }

        protected override bool IsOutside(Vector3 objectTransformPosition)
        {
            return (objectTransformPosition - GetLocalGravityCenter(objectTransformPosition)).magnitude > minorRadius;
        }
        
    }
}