﻿using UnityEngine;

namespace MagneticSurfaces.SurfaceImplementations
{
    public class MagneticCone : MagneticSurface
    {
        private Vector3 axis => Vector3.up;
        public Vector3 lineDirection;
        public float height = 1f;
        private int _verticalResolution = 2;

        protected override Vector3[] Vertices()
        {
            Vector3[] verts = new Vector3[vertexGen_n * vertexGen_n];
            for (int i = 0; i < vertexGen_n; i++)
            {
                float majorAngle = 360f * i / vertexGen_n;
                Quaternion q = Quaternion.AngleAxis(majorAngle, axis);
                for (int j = 0; j < _verticalResolution; j++)
                {
                    float l = height * ((float)j / (_verticalResolution - 1f));
                    verts[vertexGen_n * i + j] = (q * lineDirection) * l;
                }
            }

            return verts;
        }

        protected override int[] Triangles()
        {
            int[] tris = new int[6 * (vertexGen_n * (vertexGen_n))];
            for (int i = 0; i < vertexGen_n; i++)
            {
                for (int j = 0; j < _verticalResolution - 1; j++)
                {
                    //wrap on i, no wrap on j
                    int idx = 6 * (vertexGen_n * i + j);
                    System.Array.Copy(getQuad(i, j, vertexGen_n, _verticalResolution), 0, tris, idx, 6);
                    // source,source-index,dest,dest-index,count
                }
            }

            return tris;
        }

        protected internal override Vector3 SurfaceNormal(Vector3 objectTransformPosition)
        {
            return NormalFromLineDir(CoplanarLineDirection(objectTransformPosition));
        }

        protected internal override Vector3 MapToEdge(Vector3 objectTransformPosition, float offset)
        {
            Vector3 pointOnSurface = PointOnSurface(objectTransformPosition);
            if (GameManager.IsOutside())
            {
                offset *= -1;
            }

            return pointOnSurface - NormalFromLineDir(CoplanarLineDirection(objectTransformPosition)) * offset;
        }

        protected override bool IsOutside(Vector3 objectTransformPosition)
        {
            float diff = Vector3.ProjectOnPlane(objectTransformPosition, axis).magnitude -
                         Vector3.ProjectOnPlane(PointOnSurface(objectTransformPosition), axis).magnitude;
            return diff > 0;
        }

        private Vector3 PointOnSurface(Vector3 transformPosition)
        {
            Vector3 normal = NormalFromLineDir(CoplanarLineDirection(transformPosition));
            Vector3 direction = transformPosition - center;
            return Vector3.ProjectOnPlane(direction, normal);
        }

        private Vector3 NormalFromLineDir(Vector3 newLineDir)
        {
            return Vector3.Cross(Vector3.Cross(newLineDir, axis), newLineDir).normalized;
        }

        private Vector3 CoplanarLineDirection(Vector3 transformPosition)
        {
            //TODO fix ball not rolling onto the other cone

            if ((transformPosition - center).magnitude == 0)
            {
                throw new System.NotImplementedException();
            }

            Vector3 direction = transformPosition - center;
            Vector3 axisToBall = Vector3.ProjectOnPlane(direction, axis);
            Vector3 axisToLineDir = Vector3.ProjectOnPlane(lineDirection, axis);
            Quaternion q = Quaternion.FromToRotation(axisToLineDir.normalized, axisToBall.normalized);
            return (q * lineDirection).normalized;
        }
    }
}