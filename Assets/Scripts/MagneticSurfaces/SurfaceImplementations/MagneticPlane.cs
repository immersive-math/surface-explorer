using UnityEngine;

namespace MagneticSurfaces.SurfaceImplementations
{
    public class MagneticPlane : MagneticSurface
    {
        public float sizeMultiplier;
        
        protected override Vector3[] Vertices()
        {
            Vector3[] verts = new Vector3[vertexGen_n * vertexGen_n];
            
            float half = vertexGen_n / 2;
            
            for (int i = 0; i < vertexGen_n; i++)
            {
                for (int j = 0; j < vertexGen_n; j++)
                {
                    verts[vertexGen_n * i + j] = (Vector3.right * (-half + j) + Vector3.forward * (-half + i)) * sizeMultiplier;
                }
            }

            return verts;
        }

        protected override int[] Triangles()
        {
            int[] tris = new int[6 * (vertexGen_n * (vertexGen_n))];
            for (int i = 0; i < vertexGen_n; i++)
            {
                for (int j = 0; j < vertexGen_n - 1; j++)
                {
                    //wrap on i, not on j
                    int idx = 6 * (vertexGen_n * i + j);
                    System.Array.Copy(getQuad(i, j, vertexGen_n), 0, tris, idx, 6);
                    // source,source-index,dest,dest-index,count
                }
            }

            return tris;
        }

        protected internal override Vector3 SurfaceNormal(Vector3 objectTransformPosition)
        {
            if (objectTransformPosition.y < center.y)
            {
                return Vector3.down;
            }
            return Vector3.up;
        }

        protected internal override Vector3 MapToEdge(Vector3 objectTransformPosition, float offset)
        {
            if (GameManager.IsOutside())
            {
                offset *= -1;
            }
            return Vector3.ProjectOnPlane(objectTransformPosition, Vector3.up) + Vector3.up * offset;
        }

        protected override bool IsOutside(Vector3 objectTransformPosition)
        {
            return true;
        }
    }
}
