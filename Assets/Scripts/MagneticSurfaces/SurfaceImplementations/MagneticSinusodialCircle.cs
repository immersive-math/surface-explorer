using System.Numerics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace MagneticSurfaces.SurfaceImplementations
{
    public class MagneticSinusodialCircle : MagneticSurface
    {
        public float radius = 1f;
        public float height = 1f;
        private Vector3 axis => this.transform.up;
        public int verticalResolution;

        public int peaks = 5;
        
        protected override Vector3[] Vertices()
        {
            Vector3[] verts = new Vector3[vertexGen_n * vertexGen_n];

            for (int i = 0; i < vertexGen_n; i++)
            {
                float angle = 2 * Mathf.PI * i / vertexGen_n;
                float x = GetX(angle);
                float y = GetY(angle);
                
                for (int j = 0; j < verticalResolution; j++)
                {
                    float l = height * (-0.5f + (float)j / (verticalResolution - 1f));
                    verts[vertexGen_n * i + j] = new Vector3(x, l, y) * radius;
                }
            }

            return verts;
        }

        protected override int[] Triangles()
        {
            int[] tris = new int[6 * (vertexGen_n * vertexGen_n)];
            for (int i = 0; i < vertexGen_n; i++)
            {
                for (int j = 0; j < vertexGen_n; j++)
                {
                    int idx = 6 * (vertexGen_n * i + j);
                    System.Array.Copy(getQuad(i, j, vertexGen_n, verticalResolution), 0, tris, idx, 6);
                    // source,source-index,dest,dest-index,count
                }
            }

            return tris;
        }

        protected internal float TangentSlope(float t)
        {
            float dx = (-Mathf.Sin(t) * Mathf.Sin(peaks * t) + peaks * Mathf.Cos(t) * Mathf.Cos(peaks * t) - 4 * Mathf.Sin(t));
            float dy = ( Mathf.Cos(t) * Mathf.Sin(peaks * t) + peaks * Mathf.Sin(t) * Mathf.Cos(peaks * t) + 4 * Mathf.Cos(t));
            float slope = dy / dx;
            
            return slope;
        }

        protected internal float NormalSlope(float tangentSlope)
        {
            return -1 / tangentSlope;
        }

        protected internal override Vector3 SurfaceNormal(Vector3 objectTransformPosition)
        {
            float slope = NormalSlope(TangentSlope(FindRadianAngle(objectTransformPosition)));
            Vector3 normalVector = new Vector3(1, 0, slope).normalized;
            if (CheckIfPointInside(objectTransformPosition + normalVector*5)) {
                normalVector *= -1;
            }
            return normalVector;
        }

        protected internal override Vector3 MapToEdge(Vector3 objectTransformPosition, float offset)
        {
            if (GameManager.IsOutside())
            {
                //outside the sphere
                offset *= -1;
            }
            
            Vector3 surfacePosition = GetSurfacePosition(objectTransformPosition);

            //Debug.Log("offset: " + offset);
            
            return surfacePosition + SurfaceNormal(surfacePosition) * -offset;
        }

        protected override bool IsOutside(Vector3 objectTransformPosition)
        {
            return true;
        }
        
        /*public override void HandleTeleport(Transform xrorigin, Vector3 position, float offset)
        {
            position = Vector3.forward * 100000;
            
            Vector3 newPos = MapToEdge(position, offset);
            Vector3 normal = SurfaceNormal(position);

            xrorigin.position = newPos;
            xrorigin.rotation = Quaternion.FromToRotation(Vector3.up, Vector3.forward);
        }*/

        protected float FindRadianAngle(Vector3 objectTransformPosition)
        {
            Vector3 projectedVector = Vector3.ProjectOnPlane(objectTransformPosition - center, axis).normalized;
            float angle = Vector3.SignedAngle(projectedVector, Vector3.right, axis);
            return (angle / 180) * Mathf.PI;
        }

        protected bool CheckIfPointInside(Vector3 objectTransformPosition)
        {
            Vector3 surfacePosition = GetSurfacePosition(objectTransformPosition);
            return Vector3.Distance(objectTransformPosition, center) < Vector3.Distance(surfacePosition, center);
        }

        protected float GetX(float angle)
        {
            return (Mathf.Sin(peaks * angle) + 4) / 4 * Mathf.Cos(angle) / 1.25f;
        }
        
        protected float GetY(float angle)
        {
            return (Mathf.Sin(peaks * angle) + 4) / 4 * Mathf.Sin(angle) / 1.25f;
        }

        protected Vector3 GetSurfacePosition(Vector3 objectTransformPosition)
        {
            float angle = FindRadianAngle(objectTransformPosition);
            
            float x = GetX(angle);
            float y = GetY(angle);
            
            return new Vector3(x * radius, objectTransformPosition.y, y * radius);
        }
        
    }
}