﻿using UnityEngine;

namespace MagneticSurfaces.SurfaceImplementations
{
    public class MagneticSphere : MagneticSurface
    {
        public float radius = 1f;

        protected override Vector3[] Vertices()
        {
            Vector3[] verts = new Vector3[vertexGen_n * vertexGen_n];

            //consider another approach for clean surface at the poles.
            for (int i = 0; i < vertexGen_n; i++)
            {
                float angle1 = 360f * i / vertexGen_n;
                Vector3 northDir = Vector3.up;
                Vector3 meridianDir = Vector3.right;
                Quaternion q1 = Quaternion.AngleAxis(angle1, northDir);
                for (int j = 0; j < vertexGen_n; j++)
                {
                    float angle2 = 180f * j / (vertexGen_n - 1f);
                    Quaternion q2 = Quaternion.AngleAxis(angle2, q1 * meridianDir);
                    verts[vertexGen_n * i + j] = q2 * q1 * Vector3.up * radius;
                }
            }

            return verts;
        }

        protected override int[] Triangles()
        {
            int[] tris = new int[6 * (vertexGen_n * (vertexGen_n))];
            for (int i = 0; i < vertexGen_n; i++)
            {
                for (int j = 0; j < vertexGen_n - 1; j++)
                {
                    //wrap on i, not on j
                    int idx = 6 * (vertexGen_n * i + j);
                    System.Array.Copy(getQuad(i, j, vertexGen_n), 0, tris, idx, 6);
                    // source,source-index,dest,dest-index,count
                }
            }

            return tris;
        }

        protected internal override Vector3 SurfaceNormal(Vector3 objectTransformPosition)
        {
            return (objectTransformPosition - center).normalized;
        }

        protected internal override Vector3 MapToEdge(Vector3 objectTransformPosition, float offset)
        {
            if (GameManager.IsOutside())
            {
                offset *= -1;
            }

            return center + (objectTransformPosition - center).normalized * (radius - offset);
        }

        protected override bool IsOutside(Vector3 objectTransformPosition)
        {
            return (objectTransformPosition - center).magnitude > radius;
        }
    }
}