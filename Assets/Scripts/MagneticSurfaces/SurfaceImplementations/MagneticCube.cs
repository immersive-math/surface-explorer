using UnityEngine;

namespace MagneticSurfaces.SurfaceImplemnetations
{
    public class MagneticCube : MagneticSurface
    {
        private MeshRenderer _meshRenderer => GetComponent<MeshRenderer>();

        private float sideLength = 1f;


        protected override Vector3[] Vertices()
        {
            Vector3[] result = new Vector3[]
            {
                new Vector3(1, 1, 1) * (sideLength / 2f),
                new Vector3(1, 1, -1) * (sideLength / 2f),
                new Vector3(-1, 1, -1) * (sideLength / 2f),
                new Vector3(-1, 1, 1) * (sideLength / 2f),
                new Vector3(1, -1, 1) * (sideLength / 2f),
                new Vector3(1, -1, -1) * (sideLength / 2f),
                new Vector3(-1, -1, -1) * (sideLength / 2f),
                new Vector3(-1, -1, 1) * (sideLength / 2f)
            };
            return result;
        }

        protected override int[] Triangles()
        {
            int[] tris = new int[]
            {
                0, 1, 2,
                0, 2, 3,
                4, 6, 5,
                4, 7, 6,
                0, 5, 1,
                0, 4, 5,
                3, 6, 7,
                3, 2, 6,
                1, 6, 2,
                1, 5, 6,
                0, 7, 4,
                0, 3, 7,
            };
            return tris;
        }

        protected internal override Vector3 SurfaceNormal(Vector3 objectTransformPosition)
        {
            float halfSide = sideLength / 2.0f;
            Vector3 gravityDirection = transform.position - center;

            if (Mathf.Abs(gravityDirection.x) > halfSide ||
                Mathf.Abs(gravityDirection.y) > halfSide ||
                Mathf.Abs(gravityDirection.z) > halfSide)
            {
                gravityDirection *= -1;
            }

            return gravityDirection;
        }

        protected internal override Vector3 MapToEdge(Vector3 objectTransformPosition, float offset)
        {
            float sphereRadius = _meshRenderer.bounds.size.x / 2;
            float halfSide = sideLength / 2.0f;
            Vector3 diff = transform.position - center;
            Vector3 diffNormalized = diff.normalized;

            if (IsOutside(objectTransformPosition))
            {
                sphereRadius *= -1;
            }

            // the factor to adjust so the unit sphere is projected onto unit cube
            float m = Mathf.Max(
                Mathf.Abs(diffNormalized.x),
                Mathf.Abs(diffNormalized.y),
                Mathf.Abs(diffNormalized.z));

            Vector3 posOnInnerSphere = diff.normalized * (halfSide - sphereRadius) + center;
            return new Vector3(
                posOnInnerSphere.x / m,
                posOnInnerSphere.y / m,
                posOnInnerSphere.z / m);
        }

        protected override bool IsOutside(Vector3 objectTransformPosition)
        {
            float sphereRadius = _meshRenderer.bounds.size.x / 2;
            float halfSide = sideLength / 2.0f;
            Vector3 diff = transform.position - center;
            return Mathf.Abs(diff.x) > halfSide ||
                   Mathf.Abs(diff.y) > halfSide ||
                   Mathf.Abs(diff.z) > halfSide;
        }
    }
}