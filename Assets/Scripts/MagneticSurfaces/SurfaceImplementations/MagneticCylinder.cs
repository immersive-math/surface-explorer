using System;
using UnityEngine;

namespace MagneticSurfaces.SurfaceImplementations
{
    public class MagneticCylinder : MagneticSurface
    {
        public float radius;

        public float height;

        //TODO fix blender output
        private Vector3 axis => this.transform.up;
        
        protected override Vector3[] Vertices()
        {
            Vector3[] verts = new Vector3[vertexGen_n * vertexGen_n];
            Vector3 secondaryAxisReference = this.transform.right;
            for (int i = 0; i < vertexGen_n; i++)
            {
                float l = height * (-0.5f + (float)i / (vertexGen_n - 1f));
                for (int j = 0; j < vertexGen_n; j++)
                {
                    float majorAngle = 360f * j / vertexGen_n;
                    Quaternion q = Quaternion.AngleAxis(majorAngle, axis);
                    verts[vertexGen_n * i + j] = q * secondaryAxisReference * radius + axis * l;
                }
            }
            return verts;
        }

        protected override int[] Triangles()
        {
            int[] tris = new int[6 * vertexGen_n * vertexGen_n];
            for (int i = 0; i < vertexGen_n; i++)
            {
                for (int j = 0; j < vertexGen_n; j++)
                {
                    //wrap on i, no wrap on j
                    int idx = 6 * (vertexGen_n * i + j);
                    System.Array.Copy(getQuad(i, j, vertexGen_n), 0, tris, idx, 6);
                    // source,source-index,dest,dest-index,count
                }
            }

            return tris;
        }

        protected internal override Vector3 SurfaceNormal(Vector3 objectTransformPosition)
        {
            return Vector3.ProjectOnPlane(objectTransformPosition - center, axis).normalized;
        }

        protected internal override Vector3 MapToEdge(Vector3 objectTransformPosition, float offset)
        {
            if (GameManager.IsOutside())
            {
                //outside the sphere
                offset *= -1;
            }

            return center + Vector3.Project(objectTransformPosition - center, axis) +
                   Vector3.ProjectOnPlane(objectTransformPosition - center, axis).normalized * (radius - offset);
        }

        protected override bool IsOutside(Vector3 objectTransformPosition)
        {
            return Vector3.ProjectOnPlane(objectTransformPosition - center, axis).magnitude > radius;
        }
        
        /*// TODO: Find a better way to handle teleporting between torus and cylinder
         Possibly done
        // Cylinder teleport positions needs to be within limits whereas Torus need to be large
        public override void HandleTeleport(Transform xrorigin, Vector3 position, float offset)
        {
            position /= 1000;
            
            Vector3 newPos = MapToEdge(position, offset);
            Vector3 normal = AdjustedSurfaceNormal(newPos);

            xrorigin.position = newPos;
            xrorigin.rotation = Quaternion.FromToRotation(Vector3.up, normal);
            Debug.Log(position + " " + newPos);
        }*/
    }
}