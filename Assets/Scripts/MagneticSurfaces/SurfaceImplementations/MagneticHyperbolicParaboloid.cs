using UnityEngine;

namespace MagneticSurfaces
{
    public class MagneticHyperbolicParaboloid : MagneticSurface
    {
        // the general formula for hyperbolic paraboloid is:
        // y = (z^2 / b^2) - (x^2 / a^2) 
        // unity uses y axis as up

        public float a;
        public float b;
        public float xBound;
        public float zBound;

        private LineRenderer lr;

        protected override Vector3[] Vertices()
        {
            lr = GetComponent<LineRenderer>();
            Vector3[] verts = new Vector3[vertexGen_n * vertexGen_n];

            for (int i = 0; i < vertexGen_n; i++)
            {
                float x = -xBound + (xBound * 2) / (vertexGen_n - 1) * i;

                for (int j = 0; j < vertexGen_n; j++)
                {
                    float z = -zBound + (zBound * 2) / (vertexGen_n - 1) * j;
                    float y = (z * z) / (b * b) - (x * x) / (a * a);
                    Vector3 pointOnSurface = new Vector3(x, y, z);
                    verts[vertexGen_n * i + j] = pointOnSurface;
                    /*  Draws the normals of the surface
                        lr.SetPosition((vertexGen_n * i + j) * 3, pointOnSurface);
                        lr.SetPosition((vertexGen_n * i + j) * 3 + 1, pointOnSurface + SurfaceNormal(pointOnSurface));
                        lr.SetPosition((vertexGen_n * i + j) * 3 + 2, pointOnSurface);
                    */
                }
            }

            return verts;
        }

        protected override int[] Triangles()
        {
            int[] tris = new int[6 * (vertexGen_n) * (vertexGen_n)];
            for (int i = 0; i < vertexGen_n - 1; i++)
            {
                for (int j = 0; j < vertexGen_n - 1; j++)
                {
                    int idx = 6 * (vertexGen_n * i + j);
                    System.Array.Copy(getQuad(i, j, vertexGen_n), 0, tris, idx, 6);
                    // source,source-index,dest,dest-index,count
                }
            }

            return tris;
        }

        protected internal override Vector3 SurfaceNormal(Vector3 objectTransformPosition)
        {
            float x = objectTransformPosition.x;
            float z = objectTransformPosition.z;
            return new Vector3((2 * x) / (a * a), 1, (-2 * z) / (b * b)).normalized;
        }

        protected internal override Vector3 MapToEdge(Vector3 objectTransformPosition, float offset)
        {
            Vector3 pointOnSurface = new Vector3(
                objectTransformPosition.x,
                (objectTransformPosition.z * objectTransformPosition.z) / (b * b) -
                (objectTransformPosition.x * objectTransformPosition.x) / (a * a),
                objectTransformPosition.z);
            Vector3 normal = SurfaceNormal(pointOnSurface);
            return pointOnSurface + normal * offset;
        }

        protected override bool IsOutside(Vector3 objectTransformPosition)
        {
            return true;
        }

        /*protected internal Vector3 GetClosestSurfacePoint(Vector3 objectTransformPosition)
        {

        }*/

        new void Start()
        {
            base.Start();
            lr = GetComponent<LineRenderer>();
            /*float _x = 15;
            float _z = 15;
            Vector3 pointOnSurface = new Vector3(_x, (_x * _x) / (b * b) - (_z * _z) / (a * a), _z);
            lr.SetPosition(0, pointOnSurface);
            lr.SetPosition(1, pointOnSurface + SurfaceNormal(pointOnSurface));*/
        }
    }
}