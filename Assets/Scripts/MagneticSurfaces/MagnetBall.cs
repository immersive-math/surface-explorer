using System.Collections;
using System.Collections.Generic;
using SurfaceExplorer;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.XR;
using CommonUsages = UnityEngine.XR.CommonUsages;
using InputDevice = UnityEngine.XR.InputDevice;

namespace MagneticSurfaces
{
    public class MagnetBall : MonoBehaviour
    {
        [FormerlySerializedAs("rollBall")] public InputAction rollBallAction;
        public float speed = 0f;
        
        private readonly float _gravityConst = 9.81f;

        public MagneticSurface surface;
        private Rigidbody _rigidbody;
        private TrailRenderer _trailRenderer;
        private LineRenderer _lineRenderer;
        private InterpolatedScale _ip;
        private InputDevice _controller;
        
        // Controller variables
        private Vector3 _controllerPosition;
        private Quaternion _controllerRotation;

        // state control variables
        public bool isReleased;
        public bool gravityAnimationEnabled;
        
        // Physics calculations
        private Vector3 _velocity;
        private Vector3 _normal;
        private Vector3 _spinningAxis;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.isKinematic = true;
            _trailRenderer = GetComponent<TrailRenderer>();
            _lineRenderer = GetComponent<LineRenderer>();
            surface = FindObjectOfType<MagneticSurface>().GetComponent<MagneticSurface>();
            _velocity = _rigidbody.velocity;
            /*rollBallAction.canceled += Roll;
            rollBallAction.Enable();*/
            GetComponent<TrailRenderer>().enabled = false;
            SetRandomColor();
            GameManager.Instance.toggleTrailsEvent.AddListener(ToggleTrails);
           _ip = GetComponent<InterpolatedScale>();
        }

        void LateUpdate()
        {
            if (!isReleased)
            {
                return;
            }

            if (gravityAnimationEnabled)
            {
                if (CheckForProximity(GetRadius()*3))
                {
                    gravityAnimationEnabled = false;
                    StartCoroutine(LerpVelocity(2f));
                    StartCoroutine(_ip.InflateBall());
                }
                else
                {
                    ApplyGravity();
                }

                return;
            }

            AdjustPositionOnSurface();
        }

        void FixedUpdate()
        {
            if (isReleased)
            {
                return;
            }
            
            Vector3 position = default;
            Quaternion rotation = default;
            
            if (_controller.TryGetFeatureValue(CommonUsages.devicePosition, out position) &&
                _controller.TryGetFeatureValue(CommonUsages.deviceRotation, out rotation))
            {
                _controllerPosition = GameManager.GetXROriginPosition() + GameManager.GetXROriginRotation() * position;
                _controllerRotation = GameManager.GetXROriginRotation() * rotation;
                Vector3 ballPosition = _controllerPosition + _controllerRotation * new Vector3(0, 0, 0.2f);
                
                _velocity = _controllerRotation * Vector3.down * 10f;
                
                _rigidbody.MovePosition(ballPosition);
                
                _lineRenderer.SetPosition(0, ballPosition);
                _lineRenderer.SetPosition(1, ballPosition + _controllerRotation * new Vector3(0, -0.35f, 0));
            }
        }

        private void AdjustPositionOnSurface()
        {
            float ballRadius = GetRadius();
            Vector3 newPosition = transform.position - (_normal * ballRadius) + _velocity * Time.deltaTime;
            Vector3 posOnSurface = surface.MapToEdge(newPosition, 0);
            
            // When we start rolling the ball, the normal should not be set
            // At that moment, set the spinning axis
            if (_normal.magnitude == 0)
            {
                _normal = surface.SurfaceNormal(posOnSurface);
                _spinningAxis = Vector3.Cross(_normal, _velocity);
                // _spinningAxis = Vector3.up;
                _velocity = Vector3.ProjectOnPlane(_velocity, _normal).normalized * speed;
            }
            else
            {
                _normal = surface.SurfaceNormal(posOnSurface);
            }

            newPosition = surface.MapToEdge(posOnSurface, ballRadius);
            _velocity = Vector3.ProjectOnPlane(_velocity, _normal).normalized * speed;
            // AdjustVelocity();

            //TODO: figure out a way to avoid 0 velocity without destroying the object
            if (_velocity.magnitude == 0)
            {
                Destroy(gameObject);
            }
            else if (_velocity.magnitude < 1)
            {
                //Debug.Log(_velocity);
                //Debug.LogWarning("0 velocity");
            }
            else
            {
                /*GetComponent<LineRenderer>().SetPosition(0, newPosition);
                GetComponent<LineRenderer>().SetPosition(1, newPosition + 10f * normal);*/
            }

            transform.position = newPosition;
            transform.Rotate(_spinningAxis, 1f);
        }

        private void AdjustVelocity()
        {
            Vector3 projected = Vector3.ProjectOnPlane(_velocity, _normal);
            float angle = Vector3.SignedAngle(_velocity, projected, _spinningAxis);
            _velocity = (Quaternion.AngleAxis(angle, _spinningAxis) * _velocity).normalized * speed;
        }

        private void ApplyGravity()
        {
            Vector3 newPosition = transform.position + _velocity * Time.deltaTime;
            _velocity -= surface.AdjustedSurfaceNormal(transform.position) * (_gravityConst * Time.deltaTime);
            transform.position = newPosition;
        }

        private IEnumerator LerpVelocity(float duration)
        {
            float startTime = Time.time;
            float initSpeed = _velocity.magnitude;
            float targetSpeed = speed;
            speed = initSpeed;

            while (speed < targetSpeed)
            {
                speed = Mathf.Lerp(initSpeed, targetSpeed, (Time.time - startTime) / duration);
                yield return null;
            }
        }

        private bool CheckForProximity(float tolerance)
        {
            Vector3 closestSurfacePoint = surface.MapToEdge(transform.position, 0f);
            if (Mathf.Abs((transform.position - closestSurfacePoint).magnitude) < tolerance)
            {
                return true;
            }

            return false;
        }

        void Roll(InputAction.CallbackContext context)
        {
            isReleased = true;
            // SetRandomSpeed();
            //transform.position = surface.MapToEdge(transform.position, GetRadius());
            surface.balls.Add(transform);
            rollBallAction.Disable();
            gravityAnimationEnabled = true;
        }
        
        public void Shoot()
        {
            _lineRenderer.enabled = false;
            isReleased = true;
            surface.balls.Add(transform);
            rollBallAction.Disable();
            gravityAnimationEnabled = true;
        }
        
        void SetRandomColor()
        {
            Color color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            GetComponent<MeshRenderer>().material.color = color;
            GetComponent<TrailRenderer>().startColor = color;
            GetComponent<TrailRenderer>().endColor = color;
            GetComponent<LineRenderer>().startColor = color;
            GetComponent<LineRenderer>().endColor = color;
        }

        void SetRandomSpeed()
        {
            /*float scalar = 3000f;
            // speed = Random.Range(5f, 20f) * scalar;
            speed = scalar;*/
        }

        private void ToggleTrails()
        {
            _trailRenderer.enabled = !_trailRenderer.enabled;
        }

        private float GetRadius()
        {
            return transform.localScale.x / 2f;
        }

        public void SetController(InputDevice device)
        {
            _controller = device;
        }

        public void SetVelocity(Vector3 velocity)
        {
            _velocity = velocity;
        }

        public void EnableTrailIfGloballyEnabled()
        {
            _trailRenderer.enabled = GameManager.trailsEnabled;
        }
    }
}