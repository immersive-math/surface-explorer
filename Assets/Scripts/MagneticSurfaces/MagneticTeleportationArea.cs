using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace MagneticSurfaces
{
    /// <summary>
    /// An area is a teleportation destination which teleports the user to their pointed
    /// location on a surface, rewritten for magnetic surfaces
    /// </summary>
    [RequireComponent(typeof(MagneticSurface))]
    public class MagneticTeleportationArea : TeleportationArea 
    {
        
        /// <inheritdoc />
        protected override bool GenerateTeleportRequest(IXRInteractor interactor, RaycastHit raycastHit, ref TeleportRequest teleportRequest)
        {
            Debug.Log("bing");
            GetComponent<MagneticSurface>().HandleTeleport(GameManager.Instance.XROrigin, raycastHit.point, 0.2f);
            return false;
        }
    }
}