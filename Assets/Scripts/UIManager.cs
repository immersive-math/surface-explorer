using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Button nextSurfaceButton;
    public Button previousSurfaceButton;
    
    public void SetNextSurfaceButtonState()
    {
        if (GameManager.GetSurfaceIdx() >= GameManager.GetSurfaceCount() - 1)
        {
            nextSurfaceButton.interactable = false;
        }
        else
        {
            nextSurfaceButton.interactable = true;
        }
    }
    
    public void SetPreviousSurfaceButtonState()
    {
        if (GameManager.GetSurfaceIdx() == 0)
        {
            previousSurfaceButton.interactable = false;
        }
        else
        {
            previousSurfaceButton.interactable = true;
        }
    }
    
    public static void SetButtonInteractable(Button button, bool interactable)
    {
        button.interactable = interactable;
    }
}
