using System;
using System.Collections.Generic;
using System.Linq;
using FishNet;
using SurfaceExplorer;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.XR;
using CommonUsages = UnityEngine.XR.CommonUsages;
using InputDevice = UnityEngine.XR.InputDevice;

namespace MagneticSurfaces
{
    public class SpawnBall : MonoBehaviour
    {
        public GameObject magnetBallPrefab;
        public InputAction spawnBall;
        
        private GameObject _currentRightBall;
        private GameObject _currentLeftBall;

        private void Start()
        {
            spawnBall.performed += Spawn;
            spawnBall.Enable();
        }

        public void Spawn(InputAction.CallbackContext context)
        {
            InputDevice device = GetInputDevice(context);
            
            if ((device.characteristics & InputDeviceCharacteristics.Right) != 0 && _currentRightBall != null)
            {
                _currentRightBall.GetComponent<MagnetBall>().Shoot();
                _currentRightBall = null;
                return;
            }
            if ((device.characteristics & InputDeviceCharacteristics.Left) != 0 && _currentLeftBall != null)
            {
                _currentLeftBall.GetComponent<MagnetBall>().Shoot();
                _currentLeftBall = null;
                return;
            }
            
            Vector3 position = default;
            if (device.TryGetFeatureValue(CommonUsages.devicePosition, out position))
            {
                
                // Spawn ball
                magnetBallPrefab.transform.localScale = magnetBallPrefab.GetComponent<InterpolatedScale>().initialScale;
                GameObject ball = Instantiate(magnetBallPrefab, position, Quaternion.identity);
                
                // Attach ball object to the BallManager
                ball.transform.parent = transform;
                
                // Set the device in MagnetBall
                ball.GetComponent<MagnetBall>().SetController(device);

                if ((device.characteristics & InputDeviceCharacteristics.Right) != 0)
                {
                    _currentRightBall = ball;
                }
                else if ((device.characteristics & InputDeviceCharacteristics.Left) != 0)
                {
                    _currentLeftBall = ball;
                }

                if (GameManager.Instance.GetNetworked())
                {
                    InstanceFinder.ServerManager.Spawn(ball, null);
                }
            }
        }

        InputDevice GetInputDevice(InputAction.CallbackContext context)
        {
            var devices = new List<InputDevice>();
            UnityEngine.InputSystem.InputDevice id = context.control.device;
            
            if (id.usages.Contains(new InternedString("LeftHand")))
            {
                InputDevices.GetDevicesAtXRNode(XRNode.LeftHand, devices);
            }
            else if (id.usages.Contains(new InternedString("RightHand")))
            {
                InputDevices.GetDevicesAtXRNode(XRNode.RightHand, devices);
            }
            else
            {
                throw new Exception("Invalid controller type (not right or left hand)");
            }
            
            return devices[0];
        }
    }
}