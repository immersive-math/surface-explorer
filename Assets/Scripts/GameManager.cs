﻿using System;
using System.Collections.Generic;
using MagneticSurfaces;
using MagneticSurfaces.SurfaceImplementations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour, ActionMap.IPlayerActions, ActionMap.IUIbuttonsActions, ActionMap.ITeleportationActions
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    [SerializeField] private bool networked;
    public List<GameObject> surfaces = new List<GameObject>();
    private int _surfaceIdx = 0;

    public float surfaceOffset = 100f;

    public bool _isOutside;

    public GameObject compassArrow;
    public SpawnBall spawnBall;
    private AdjustCompass adjustCompass;
    public GameObject menuCanvas;
    public UIManager uiManager;
    
    public UnityEvent toggleTrailsEvent;
    public static bool trailsEnabled;
    public UnityEvent toggleNeighborhoodsEvent;
    public static bool neighborhoodsEnabled;
    public static bool sampleSurfacesEnabled;
    public UnityEvent toggleTelescopeEvent;
    public static bool telescopeEnabled;
    public static bool teleportationEnabled;
    
    public Transform XROrigin;
    public Transform RightController;
    public Transform LeftController;
    public Transform MainCamera;
    
    public ActionMap controls;

    public GameObject sampleSurfaces;
    public GameObject telescope;
    public GameObject compass;
    public GameObject teleportationInteractor;
    
    private Vector3 initialPlayerPosition;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        if (toggleTrailsEvent == null)
            toggleTrailsEvent = new UnityEvent();
        
        if (toggleNeighborhoodsEvent == null)
            toggleNeighborhoodsEvent = new UnityEvent();
        
        if (toggleTelescopeEvent == null)
            toggleTelescopeEvent = new UnityEvent();
        
        adjustCompass = compassArrow.GetComponent<AdjustCompass>();
        EnableSurface();
    }
    
    public void OnEnable()
    {
        if (controls == null)
        {
            controls = new ActionMap();
            controls.Player.SetCallbacks(this);
            controls.UIbuttons.SetCallbacks(this);
            controls.Teleportation.SetCallbacks(this);
        }
        //controls.Player.Enable();
    }

    public void OnDisable()
    {
        controls.Player.Disable();
        controls.UIbuttons.Disable();
        controls.Teleportation.Disable();
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ToggleTrails()
    {
        toggleTrailsEvent.Invoke();
        trailsEnabled = !trailsEnabled;
    }

    public void EnableNextSurface()
    {
        if (_surfaceIdx >= surfaces.Count - 1) return;
        surfaces[_surfaceIdx].SetActive(false);
        _surfaceIdx++;
        EnableSurface();
    }
    
    public void EnablePreviousSurface()
    {
        if (_surfaceIdx == 0) return;
        surfaces[_surfaceIdx].SetActive(false);
        _surfaceIdx--;
        EnableSurface();
    }
    
    private void EnableSurface()
    {
        ExitUIState(); // Go back to player control schema
        if (sampleSurfacesEnabled) { ToggleSampleSurfaces();} // Close sample surfaces
        trailsEnabled = false; // Disable trails
        neighborhoodsEnabled = false; // Disable neighborhoods
        surfaces[_surfaceIdx].SetActive(true); // Set the current surface active
        TeleportToNewPosition(0.2f); // Teleport the player
        compass.transform.position = XROrigin.position + XROrigin.transform.up * 0.005f; // Adjust the compass
        compass.transform.rotation = XROrigin.rotation;
        ClearAllBalls(); // Delete all the old balls
        uiManager.SetNextSurfaceButtonState(); /* Disable/Enable the relevant buttons */ 
        uiManager.SetPreviousSurfaceButtonState();
    }

    public void ClearAllBalls()
    {
        foreach (MagnetBall magnetBall in FindObjectsOfType<MagnetBall>())
        {
            Destroy(magnetBall.gameObject);
        }
        surfaces[_surfaceIdx].GetComponent<MagneticSurface>().RefreshBallList();
    }

    private void TeleportToNewPositionOnSurface(InputAction.CallbackContext context)
    {
        TeleportToNewPosition(0);
    }

    private void TeleportToNewPositionOffSurface(InputAction.CallbackContext context)
    {
        TeleportToNewPosition(surfaceOffset);
    }

    private void TeleportToNewPosition(float offset)
    {
        MagneticSurface surface = FindObjectOfType<MagneticSurface>();
        
        if (surface.name == "Torus")
        {
            surface.HandleTeleport(XROrigin, Vector3.forward * 50000f, offset);
            return;
        }
        
        if (surface.name == "Sinusodial Circle")
        {
            surface.HandleTeleport(XROrigin, Vector3.forward, offset);
            return;
        }
        
        surface.HandleTeleport(XROrigin, new Vector3(0, 100, 10), offset);
        initialPlayerPosition = XROrigin.position;
    }

    public bool GetNetworked() { return _instance.networked; }

    public static Vector3 GetXROriginPosition() { return _instance.XROrigin.position; }

    public static Vector3 GetRightControllerPosition() { return _instance.RightController.position; }
    
    public static Vector3 GetLeftControllerPosition() { return _instance.LeftController.position; }
    
    public static Quaternion GetXROriginRotation() { return _instance.XROrigin.rotation; }
    
    public static bool IsOutside() { return _instance._isOutside; }
    
    public static int GetSurfaceIdx() { return _instance._surfaceIdx; }
    
    public static int GetSurfaceCount() { return _instance.surfaces.Count;}

    public void ToggleSampleSurfaces()
    {
        sampleSurfacesEnabled = !sampleSurfacesEnabled;
        sampleSurfaces.transform.position = XROrigin.position + XROrigin.forward * 0.7f + XROrigin.up * 1f;
        sampleSurfaces.transform.rotation = XROrigin.rotation;
        sampleSurfaces.SetActive(sampleSurfacesEnabled);
    }
    
    /* ----- Action map callbacks ----- */

    public void OnSpawnBall(InputAction.CallbackContext context)
    {
        if (context.performed)
            spawnBall.Spawn(context);
    }

    public void OnOpenUI(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            controls.UIbuttons.Enable();
            controls.Player.Disable();

            RectTransform rectTransform = menuCanvas.GetComponent<RectTransform>();
            rectTransform.position = MainCamera.position + MainCamera.rotation * new Vector3(0, 0, 2f);
            rectTransform.rotation = MainCamera.rotation;
        
            SetUIMenuComponents(true);
        }
    }

    public void OnExitUI(InputAction.CallbackContext context)
    {
        if(context.performed)
            ExitUIState();
    }

    public void OnIncrement(InputAction.CallbackContext context)
    {
        Debug.Log("increment");
        if (context.performed)
        {
            if (TelescopeManager.Instance._telescopeEnabled)
            {
                TelescopeManager.Instance.IncreaseZoom();
            }
        }
    }

    public void OnDecrement(InputAction.CallbackContext context)
    {
        Debug.Log("decrement");
        if (context.performed)
        {
            if (TelescopeManager.Instance._telescopeEnabled)
            {
                TelescopeManager.Instance.DecreaseZoom();
            }
        }
    }

    public void OnEnableTeleportation(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ToggleTeleportation();
            controls.Player.Disable();
            controls.Teleportation.Enable();
        }
    }
    
    public void OnExitTeleportation(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ToggleTeleportation();
            controls.Teleportation.Disable();
            controls.Player.Enable();
        }
    }

    /* ------------------------------- */

    private void SetUIMenuComponents(bool state)
    {
        menuCanvas.SetActive(state);
        
        LeftController.GetComponent<XRRayInteractor>().enabled = state;
        RightController.GetComponent<XRRayInteractor>().enabled = state;
        LeftController.GetComponent<LineRenderer>().enabled = state;
        RightController.GetComponent<LineRenderer>().enabled = state;
    }

    private void ExitUIState()
    {
        controls.UIbuttons.Disable();
        controls.Player.Enable();
        SetUIMenuComponents(false);
    }

    public void ToggleTelescope()
    {
        toggleTelescopeEvent.Invoke();
        telescopeEnabled = !telescopeEnabled;
        telescope.SetActive(telescopeEnabled);
    }
    
    public void ToggleTeleportation()
    {
        teleportationEnabled = !teleportationEnabled;
        teleportationInteractor.SetActive(teleportationEnabled);
    }

    public void ResetSurfacePosition()
    {
        MagneticSurface surface = FindObjectOfType<MagneticSurface>();
        surface.HandleTeleport(XROrigin, initialPlayerPosition, 0.2f);
        ExitUIState();
    }
}