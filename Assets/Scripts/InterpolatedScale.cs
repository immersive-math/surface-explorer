﻿using System.Collections;
using System.Collections.Generic;
using MagneticSurfaces;
using UnityEngine;

namespace SurfaceExplorer
{
    public class InterpolatedScale : MonoBehaviour
    {
        public Vector3 initialScale;
        public Vector3 targetScale;
        public float duration;
        
        public IEnumerator InflateBall()
        {
            float elapsedTime = 0;

            while (elapsedTime < duration)
            {
                transform.localScale = Vector3.Lerp(initialScale, targetScale, elapsedTime / duration);
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            GetComponent<MagnetBall>().EnableTrailIfGloballyEnabled();
            Destroy(this);
        }
    }
}